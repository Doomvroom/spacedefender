﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;
	public float width = 10f;
	public float height = 5f;
	public float spawnDelay = 0.5f;
	bool moveLeft;

	public float speed = 10.0f;
	float xmax;
	float xmin;

	void Start () {

		calculateEdges();
		SpawnUntilFull();
	}

	void SpawnEnemies(){
		foreach (Transform child in transform){
			GameObject enemy = Instantiate(enemyPrefab, child.transform.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = child;
		}
	}

	void SpawnUntilFull(){
		Transform openPosition = NextFreePosition();
		if( openPosition){
			GameObject enemy = Instantiate(enemyPrefab, openPosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = openPosition;
		}
		if(NextFreePosition()) Invoke ("SpawnUntilFull", spawnDelay);
	}

	void calculateEdges(){

		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance));
		Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));

		xmin = leftMost.x + width * .4f;
		xmax = rightMost.x - width * .4f;
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
	}

	void Update () {
		if(moveLeft)
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		else
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}

		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		transform.position = new Vector3(newX, transform.position.y, 0);

		if(transform.position.x <= xmin || transform.position.x >= xmax) moveLeft = !moveLeft;

		if(AllMembersDead()){
			SpawnUntilFull();
		}
	}

	Transform NextFreePosition(){
		foreach (Transform childPosition in transform){
			if(childPosition.childCount == 0) return childPosition;
		}


		return null;
	}

	bool AllMembersDead(){

			foreach (Transform childPosition in transform){
				if(childPosition.childCount > 0) return false;
		}
		return true;
	}
}
