﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float health = 150f;
	//public float firingRate = 0.2f;
	public GameObject projectile;
	public float projectileSpeed;
	public float shotsPerSecond = 0.5f;
	public int scoreValue = 150;
	private ScoreKeeper scoreKeeper;

	public AudioClip fireSound;
	public AudioClip deathSound;

	void Start()
	{
			scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();
	}

	void Update()
	{
		float probability = Time.deltaTime * shotsPerSecond;
		if(Random.value < probability){
			Fire();
		}

	}

	void Fire()
	{
		GameObject laser = Instantiate(projectile,  transform.position , Quaternion.identity) as GameObject;
		laser.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -projectileSpeed, 0);
		AudioSource.PlayClipAtPoint(fireSound, transform.position);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		var projectile = other.gameObject.GetComponent<Projectile>();
		if(projectile)
		{
			health -= projectile.GetDamage();
			projectile.Hit();

			if (health <= 0){
				scoreKeeper.Score(scoreValue);
				AudioSource.PlayClipAtPoint(deathSound, transform.position);
				Destroy(gameObject);
			}
		}
	}

}
