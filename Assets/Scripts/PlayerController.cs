﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 10.0f;
	public float padding = .0f;
	public float firingRate = 0.2f;
	public GameObject projectile;
	public float projectileSpeed;
	public float health = 250f;
	public AudioClip fireSound;

	float xmax;
	float xmin;
	// Use this for initialization
	void Start () {
		float distance = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftMost = Camera.main.ViewportToWorldPoint(new Vector3(0,0,distance));
		Vector3 rightMost = Camera.main.ViewportToWorldPoint(new Vector3(1,0,distance));

		xmin = leftMost.x + padding;
		xmax = rightMost.x - padding;
	}

	void Fire()
	{
			GameObject laser = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
			laser.GetComponent<Rigidbody2D>().velocity = new Vector3(0, projectileSpeed, 0);
			AudioSource.PlayClipAtPoint(fireSound, transform.position);
	}

	void Update () {

		if( Input.GetKeyDown(KeyCode.Space ))
		{
			InvokeRepeating("Fire", 0.000001f, firingRate );
		}
		if (Input.GetKeyUp(KeyCode.Space))
		{
			CancelInvoke("Fire");
		}
 		if( Input.GetKey( KeyCode.LeftArrow ) )
		 {
			transform.position += Vector3.left * speed * Time.deltaTime;
		 }
        if( Input.GetKey( KeyCode.RightArrow ) )
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}

		float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
		transform.position = new Vector3(newX, transform.position.y, 0);

	}

		void OnTriggerEnter2D(Collider2D other)
	{
		var projectile = other.gameObject.GetComponent<Projectile>();
		if(projectile)
		{
			health -= projectile.GetDamage();
			projectile.Hit();

			if (health <= 0) Die();
		}
	}

	void Die(){
		LevelManager manager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
		manager.LoadLevel("Win Screen");
		Destroy(gameObject);
	}
}
